Source: grpc
Priority: optional
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Build-Depends: debhelper (>= 10~), zlib1g-dev, libssl-dev, libabsl-dev,
 libprotobuf-dev, protobuf-compiler (>= 3.11.4~), libprotoc-dev (>= 3.11.4~), libc-ares-dev
Standards-Version: 4.5.0
Section: libs
Homepage: https://www.grpc.io/
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Vcs-Git: https://salsa.debian.org/debian/grpc.git
Vcs-Browser: https://salsa.debian.org/debian/grpc

Package: libgrpc-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libgrpc10 (= ${binary:Version}), ${misc:Depends}
Conflicts: libgpr1-dev, libgpr2-dev, libgpr3-dev
Description: high performance general RPC framework (development)
 A modern, open source remote procedure call (RPC) framework that can
 run anywhere. It enables client and server applications to communicate
 transparently, and makes it easier to build connected systems.
 .
 This package contains the headers and the static library for libgrpc10.

Package: libgrpc10
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: high performance general RPC framework
 A modern, open source remote procedure call (RPC) framework that can
 run anywhere. It enables client and server applications to communicate
 transparently, and makes it easier to build connected systems.
 .
 This package provides the gRPC C bindings.

Package: libgrpc++-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libgrpc++1 (= ${binary:Version}), libgrpc-dev (=${binary:Version}), ${misc:Depends}
Description: high performance general RPC framework (development)
 A modern, open source remote procedure call (RPC) framework that can
 run anywhere. It enables client and server applications to communicate
 transparently, and makes it easier to build connected systems.
 .
 This package contains the headers and the static library for libgrpc++1.

Package: libgrpc++1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: high performance general RPC framework
 A modern, open source remote procedure call (RPC) framework that can
 run anywhere. It enables client and server applications to communicate
 transparently, and makes it easier to build connected systems.
 .
 This package provides the gRPC C++ bindings.

Package: protobuf-compiler-grpc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, protobuf-compiler
Description: high performance general RPC framework - protobuf plugin
 A modern, open source remote procedure call (RPC) framework that can
 run anywhere. It enables client and server applications to communicate
 transparently, and makes it easier to build connected systems.
 .
 This package provides the plugins needed for compiling gRPC service
 definitions with the protobuf compiler.
